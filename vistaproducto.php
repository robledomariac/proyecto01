<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRODUCTOS</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <br><br><h1>Productos</h1> <br>
    <h3>Registrar producto</h3>
    <label for="">Nombre Producto: </label>
    <input type="text" id="nombre" name="nombre"><br>
    <label for="">Valor : </label>
    <input type="text" id="valor" name="valor"><br>
    <input type="submit" value="Registrar" id="guardar" name="guardar" onclick="guardar()"><br><br>
    <h3>Lista Productos Registrados</h3>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Valor</th>
            </tr>
        </thead>

        <tbody>
                <?php  
                include("config/conexion.php");
                $sql = "SELECT * FROM producto";
                $resul = mysqli_query($db,$sql);
                while($fila = mysqli_fetch_array($resul)){
                ?>                
                <tr>
                    <td><?php echo $fila['1'] ?></td>
                    <td><?php echo $fila['2'] ?></td>
                </tr>
                <?php
                }
                ?>   
        </tbody>

    </table>

    <script src="jquery-3.5.1.min.js"></script>

    <script>

        function guardar(){
            var nombre = $('#nombre').val();
            var valor = $('#valor').val();

            $.ajax({
                url : 'registroproducto.php',
                type : 'POST',
                data : {
                    nombre : nombre,
                    valor : valor,
                },
                success : function(e){
                    alert(e);
                }
            });

            document.getElementById("nombre").value = "";
            document.getElementById("valor").value = "";

        }

    </script>

</body>
</html>