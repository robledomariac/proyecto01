<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HISTORIAL PEDIDOS</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <h1>Historial de pedidos</h1>

    <!-- TABLA PEDIDOS -->
<table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha_Hora</th>
                <th>Ciudad</th>
                <th>Dirección</th>
                <th>Telefono</th>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Estado</th>
            </tr>
        </thead>         
        <tbody>
        <?php  
                include("config/conexion.php");
                $sql = "SELECT historialpedidos.id, cliente.nombre, historialpedidos.fechahora,
                 historialpedidos.ciudad, historialpedidos.direccion, historialpedidos.telefono,
                  producto.nombre, historialpedidos.cantidad, estado.estado 
                FROM historialpedidos 
                join cliente on historialpedidos.nombre = cliente.id 
                join producto on historialpedidos.producto = producto.id 
                join estado on historialpedidos.estado = estado.id";
                $resul = mysqli_query($db,$sql);
                while($fila = mysqli_fetch_array($resul)){
                ?>                
                <tr>
                    <td><?php echo $fila['0'] ?></td>
                    <td><?php echo $fila['1'] ?></td>
                    <td><?php echo $fila['2'] ?></td>
                    <td><?php echo $fila['3'] ?></td>
                    <td><?php echo $fila['4'] ?></td>
                    <td><?php echo $fila['5'] ?></td>
                    <td><?php echo $fila['6'] ?></td>
                    <td><?php echo $fila['7'] ?></td>
                    <td><?php echo $fila['8'] ?></td>
                </tr>
                <?php
                }
                ?>   
        </tbody>
    </table> 

</body>
</html>