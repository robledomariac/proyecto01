<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CLIENTES</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
<br><br><h1>Clientes</h1> <br>
<h3>Registrar cliente</h3>
    <br>
    <label for="">Cedula : </label>
    <input type="text" id="cedula" name="cedula"><br>
    <label for="">Nombre : </label>
    <input type="text" id="nombre" name="nombre"><br>
    <label for="">Apellido : </label>
    <input type="text" id="apellido" name="apellido"><br>
    <label for="">Telefono : </label>
    <input type="text" id="telefono" name="telefono"><br> <br>
    <input type="submit" value="Registrar" id="btnRegistro" onclick="registro()"><br> <br>
    <!-- <input type="submit" value="Mostrar" id="bntMostrar" onclick="mostrar()"> -->
    <h3>Lista Clientes Registrados</h3>
    <!-- TABLA CLIENTES -->
    <table>
        <thead>
            <tr>
                <th>Cedula</th>
                <th>Nombre</th>
                <th>Apellido</th>
            </tr>
        </thead>         
        <tbody>
        <?php  
                include("config/conexion.php");
                $sql = "SELECT * FROM cliente";
                $resul = mysqli_query($db,$sql);
                while($fila = mysqli_fetch_array($resul)){
                ?>                
                <tr>
                    <td><?php echo $fila['3'] ?></td>    
                    <td><?php echo $fila['1'] ?></td>
                    <td><?php echo $fila['2'] ?></td>
                </tr>
                <?php
                }
                ?>   
        </tbody>
    </table> 

    <script src="jquery-3.5.1.min.js"></script>

    <script>
    function registro(){
        var cedula = $('#cedula').val();
        var nombre = $('#nombre').val();
        var apellido = $('#apellido').val();
        var telefono = $('#telefono').val();

        $.ajax({
            url : 'registrocliente.php',
            type : 'POST',
            data : {
                cedula : cedula,
                nombre : nombre,
                apellido : apellido,
                telefono : telefono
            },
            success : function(e){ 
                alert(e);
            }
        });
        document.getElementById("cedula").value = "";
        document.getElementById("nombre").value = "";
        document.getElementById("apellido").value = "";
        document.getElementById("telefono").value = "";
    }
</script>
</body>
</html>