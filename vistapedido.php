<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PEDIDOS</title>
    <link rel="stylesheet" href="estilos.css">
</head>
<body>
    <br><br><h1>Pedidos</h1> <br>                                                                                         
    <div id="contenido">
        <h3>Registrar pedido</h3>
        <label for="">Cliente : </label>
        <select name="nombre" id="nombre">
                <option value="">seleccione el cliente</option>
        </select><br>
        <label for="">Ciudad : </label>
        <input type="text" name="ciudad" id="ciudad"><br>
        <label for="">Dirección : </label>
        <input type="text" name="direccion" id="direccion"><br>
        <label for="">Telefono : </label>
        <input type="text" name="telefono" id="telefono"><br>
        <label for="">Producto : </label>
        <select name="producto" id="producto">
                <option value="">seleccione el producto</option>
        </select><br>
        <label for="">Cantidad : </label>
        <input id="cantidad" name="cantidad" type="number" max="31" min="0" value="0" step="1"><br>
        <input type="submit" value="Guardar" id="guardar" name="guardar" onclick="guardar()">
    </div>
    <div id="edit" name="edit" hidden>
        <h3>Actualizar estado de pedido</h3>
        <label for="" hidden>ID : </label>
        <input type="text" name="id" id="id" hidden><br>
        <label for="">Cliente : </label>
        <input type="text" id="nombreedit" name="nombreedit"><br>
        <label for="">Ciudad : </label>
        <input type="text" name="ciudadedit" id="ciudadedit"><br>
        <label for="">Dirección : </label>
        <input type="text" name="direccionedit" id="direccionedit"><br>
        <label for="">Telefono : </label>
        <input type="text" name="telefonoedit" id="telefonoedit"><br>
        <label for="">Producto : </label>
        <input type="text" id="productoedit" name="productoedit"><br>
        <label for="">Cantidad : </label>
        <input id="cantidadedit" name="cantidadedit" type="number" max="31" min="0" value="0" step="1"><br>
        <select name="estado" id="estado">
            <option value="">cambiar estado</option>
        </select><br>
        <input type="submit" value="Actualizar" id="actualizar" name="actualizar" onclick="actualizar()"><br><br>
    </div>
    
    <h3>Lista Pedidos Registrados</h3>

<!-- TABLA PEDIDOS -->
<table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Fecha_Hora</th>
                <th>Ciudad</th>
                <th>Dirección</th>
                <th>Telefono</th>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Estado</th>
                <th>Acutalizar</th>
            </tr>
        </thead>         
        <tbody>
        <?php  
                include("config/conexion.php");
                $sql = "SELECT pedido.id, cliente.nombre, pedido.fechahora, pedido.ciudad, pedido.direccion, pedido.telefono, producto.nombre, pedido.cantidad, estado.estado 
                FROM pedido 
                join cliente on pedido.nombre = cliente.id join producto on pedido.producto = producto.id join estado on pedido.estado = estado.id";
                $resul = mysqli_query($db,$sql);
                while($fila = mysqli_fetch_array($resul)){
                ?>                
                <tr>
                    <td><?php echo $fila['0'] ?></td>
                    <td><?php echo $fila['1'] ?></td>
                    <td><?php echo $fila['2'] ?></td>
                    <td><?php echo $fila['3'] ?></td>
                    <td><?php echo $fila['4'] ?></td>
                    <td><?php echo $fila['5'] ?></td>
                    <td><?php echo $fila['6'] ?></td>
                    <td><?php echo $fila['7'] ?></td>
                    <td><?php echo $fila['8'] ?></td>
                    <td><a onclick="editar('<?php echo $fila['0']; ?>','<?php echo $fila['1']; ?>',
                    '<?php echo $fila['3']; ?>','<?php echo $fila['4']; ?>','<?php echo $fila['5']; ?>',
                    '<?php echo $fila['6']; ?>','<?php echo $fila['7']; ?>')"
                     href="#">Actualizar</a></td>
                </tr>
                <?php
                }
                ?>   
        </tbody>
    </table> 

    <a href="vistaregistropedido.php">Historial de Pedidos</a>

    <script src="jquery-3.5.1.min.js"></script>

    <script>

        // Función para listar clientes en formulario
        function listarcliente() {
            $.ajax({
                url: 'listarclientes.php',
                success: function(e) {
                    $("#nombre").append(e)
                }
            });
        }
        listarcliente()

        // Función para listar productos en formulario
        function listarproducto() {
            $.ajax({
                url: 'listarproductos.php',
                success: function(e) {
                    $("#producto").append(e)
                }
            });
        }
        listarproducto()

        // Función para listar estados en formulario
        function listarestado() {
            $.ajax({
                url: 'listarestado.php',
                success: function(e) {
                    $("#estado").append(e)
                }
            });
        }
        listarestado()


        // Función para guardar datos de pedidos
        function guardar(){
            var nombre = $('#nombre').val();
            var ciudad = $('#ciudad').val();
            var direccion = $('#direccion').val();
            var telefono = $('#telefono').val();
            var producto = $('#producto').val();
            var cantidad = $('#cantidad').val();

            $.ajax({
                url : 'registropedido.php',
                type : 'POST',
                data : {
                    nombre : nombre,
                    ciudad : ciudad,
                    direccion : direccion,
                    telefono : telefono,
                    producto : producto,
                    cantidad : cantidad
                },
                success : function(e){
                    alert(e);
                }
            });
            historialpedido();

        }

        // Función para historial pedido datos de pedidos
        function historialpedido(){
            var nombre = $('#nombre').val();
            var ciudad = $('#ciudad').val();
            var direccion = $('#direccion').val();
            var telefono = $('#telefono').val();
            var producto = $('#producto').val();
            var cantidad = $('#cantidad').val();
            var estado = '1';

            $.ajax({
                url : 'historialpedidos.php',
                type : 'POST',
                data : {
                    nombre : nombre,
                    ciudad : ciudad,
                    direccion : direccion,
                    telefono : telefono,
                    producto : producto,
                    cantidad : cantidad,
                    estado : estado
                },
                success : function(e){
                    alert(e);
                }
            });
            document.getElementById("nombre").value = "";
            document.getElementById("ciudad").value = "";
            document.getElementById("direccion").value = "";
            document.getElementById("telefono").value = "";
            document.getElementById("producto").value = "";
            document.getElementById("cantidad").value = "";
        }


        //Función para actualizar estado del pedido
        function editar(id,nombre,ciudad,direccion,telefono,producto,cantidad){
            $('#contenido').hide();
            $('#edit').show();
            $('#id').val(id);
            $('#nombreedit').val(nombre);
            $('#ciudadedit').val(ciudad);
            $('#direccionedit').val(direccion);
            $('#telefonoedit').val(telefono);
            $('#productoedit').val(producto);
            $('#cantidadedit').val(cantidad);  
        }

        // Función para guardar datos de pedidos
        function actualizar(){
            var id = $('#id').val();
            var estado = $('#estado').val();

            $.ajax({
                url : 'actualizarpedido.php',
                type : 'POST',
                data : {
                    id : id,
                    estado : estado
                },
                success : function(e){
                    alert(e);
                }
            });
            historialpedido1();
        }


            // Función para historial pedido de actualización
        // Función para registrar modificaciones de estado de pedidos
        function historialpedido1(){
            var nombre = $('#nombreedit').val();
            var ciudad = $('#ciudadedit').val();
            var direccion = $('#direccionedit').val();
            var telefono = $('#telefonoedit').val();
            var producto = $('#productoedit').val();
            var cantidad = $('#cantidadedit').val();
            var estado = $('#estado').val();

            $.ajax({
                url : 'historialpedidos1.php',
                type : 'POST',
                data : {
                    nombre : nombre,
                    ciudad : ciudad,
                    direccion : direccion,
                    telefono : telefono,
                    producto : producto,
                    cantidad : cantidad,
                    estado : estado
                },
                success : function(e){
                    alert(e);
                }
            });
            document.getElementById("id").value = "";
            document.getElementById("nombreedit").value = "";
            document.getElementById("ciudadedit").value = "";
            document.getElementById("direccionedit").value = "";
            document.getElementById("telefonoedit").value = "";
            document.getElementById("productoedit").value = "";
            document.getElementById("cantidadedit").value = "";
            document.getElementById("producto").value = "";
            document.getElementById("estado").value = "";
        }

    </script>


</body>
</html>